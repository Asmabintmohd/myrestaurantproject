-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2019 at 04:26 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myrestro`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `catId` int(11) NOT NULL,
  `catDesc` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`catId`, `catDesc`) VALUES
(1, 'Soup'),
(2, 'Appetizer'),
(3, 'Main Course'),
(4, 'Dessert');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `pwd` varchar(32) NOT NULL,
  `role` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `username`, `pwd`, `role`) VALUES
(1, 'waiter', '202cb962ac59075b964b07152d234b70', 'waiter'),
(2, 'cook', '202cb962ac59075b964b07152d234b70', 'cook'),
(3, 'busboy', '202cb962ac59075b964b07152d234b70', 'busboy'),
(4, 'host', '202cb962ac59075b964b07152d234b70', 'host');

-- --------------------------------------------------------

--
-- Table structure for table `floorplan`
--

CREATE TABLE `floorplan` (
  `tableid` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  `waiterid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `floorplan`
--

INSERT INTO `floorplan` (`tableid`, `status`, `waiterid`) VALUES
(1, 'open', 1),
(2, 'occupied', 1),
(3, 'open', 1),
(4, 'dirty', 1),
(5, 'occupied', 1),
(6, 'occupied', 1);

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `itemid` int(11) NOT NULL,
  `catid` int(11) NOT NULL,
  `desc` varchar(30) NOT NULL,
  `price` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`itemid`, `catid`, `desc`, `price`) VALUES
(1, 1, 'tomato soup', 150.00),
(2, 1, 'Chicken soup', 220.99),
(3, 1, 'corn soup', 140.00),
(4, 1, 'tom yum soup', 130.49),
(5, 2, 'Stuffed Mushrooms', 130.00),
(6, 2, 'Vegetable Samosa Chaat', 70.00),
(7, 2, 'Batata Wada', 80.00),
(8, 2, 'Chicken Chilli', 130.00),
(9, 3, 'Spicy Vegan Potato Curry.', 300.00),
(10, 3, 'Indian Eggplant-Bhurtha', 250.00),
(11, 3, 'Grilled Fish', 180.00),
(12, 3, 'Paneer Butter Masala', 290.99),
(13, 4, 'Shahi Tukda', 120.00),
(14, 4, 'Modak', 125.00),
(15, 4, 'Payasam', 130.00),
(16, 4, 'Gajar Ka Halwa', 135.00);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orderid` int(11) NOT NULL,
  `orderref` varchar(20) NOT NULL,
  `tableid` int(11) NOT NULL,
  `orderlist` text NOT NULL,
  `done` tinyint(1) NOT NULL DEFAULT '0',
  `paid` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`orderid`, `orderref`, `tableid`, `orderlist`, `done`, `paid`) VALUES
(1, '4order20190121152000', 4, 'Indian Eggplant-Bhartha-1-250.00;Stuffed Mushrooms-1-130.00;Payasam-2-130.00;Spicy Vegan Potato Curry-1-300.00;Chicken Chilli-1-130.00;Sahi Tukda-1-120.99;Paneer Butter Marsala-2-290.99;Spicy Vegan Potato Curry-1-200.00;Vegitable Samosa Chaat-1-70.00;Chicken Chilli-1-130.00;Indian Eggplant-Bhartha-1-250.00;corn soup-1-140.00;corn soup-1-140.00;Vegitable Samosa Chaat-1-70.00;', 1, 1),
(3, '3order20190121152339', 3, 'Payasam-2-130.00;', 1, 1),
(4, '2order20190121164145', 2, 'Chicken soup-1-220.99;Grilled Fish-1-180;Gajar ka Halwa-1-135.00;', 1, 1),
(5, '2order20190121193729', 2, 'tomato soup-4-150.00;corn Soup-4-140.00;', 1, 1),
(6, '4order20190121220226', 4, 'corn soup-1-140.00;Salsa Paneer Butter Masala-3-290.99;', 1, 1),
(7, '3order20190121220301', 3, 'Stuffed Mashrooms-1-130.00;Payasam-1-130.00;', 1, 1),
(8, '5order20190121221114', 5, '', 1, 1),
(9, '5order20190121221642', 5, '', 1, 1),
(10, '5order20190121222357', 5, 'corn soup-1-140.00;Grilled Fish-1-180.00;', 1, 1),
(11, '6order20190121222432', 6, 'Grilled Fish-2-180.00;Batatawada-5-80.00;', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`catId`),
  ADD UNIQUE KEY `catId` (`catId`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `floorplan`
--
ALTER TABLE `floorplan`
  ADD PRIMARY KEY (`tableid`),
  ADD UNIQUE KEY `tableid` (`tableid`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`itemid`),
  ADD UNIQUE KEY `itemid` (`itemid`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderid`),
  ADD UNIQUE KEY `orderid` (`orderid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `catId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `floorplan`
--
ALTER TABLE `floorplan`
  MODIFY `tableid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `itemid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `orderid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
