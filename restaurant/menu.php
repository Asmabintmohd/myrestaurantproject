<?php
	$pageName = basename($_SERVER['PHP_SELF']);
	include ("./includes/employee.php"); 
	$currentUser = new Employee;
	if($currentUser->getRole($_SESSION['uName']) == "waiter") {
?
<div class="collapse navbar-collapse navbar-ex1-collapse">
	<ul class="nav navbar-nav side-nav">
		<li <?php if($pageName == "table1.php") echo "class='active'" ?>>
			<a href="table1.php"> Table Status</a>
		</li>
		<li <?php if($pageName == "payment.php") echo "class='active'" ?>>
			<a href="payment.php"> Payment</a>
		</li>
	</ul>
</div>

<?php 
}
if($currentUser->getRole($_SESSION['uName']) == "cook") {
?>
<div class="collapse navbar-collapse navbar-ex1-collapse">
	<ul class="nav navbar-nav side-nav">
		<li <?php if($pageName == "order.php") echo "class='active'" ?>>
			<a href="order.php"> Order Queue</a>
		</li>
	</ul>
</div>
<?php 
}
if($currentUser->getRole($_SESSION['uName']) == "busboy" || $currentUser->getRole($_SESSION['uName']) == "host") {
?>
<div class="collapse navbar-collapse navbar-ex1-collapse">
	<ul class="nav navbar-nav side-nav" id=<?php echo "'".$currentUser->getRole($_SESSION['uName'])."'";?>>
		<li <?php if($pageName == "table2.php") echo "class='active'" ?>>
			<a href="table2.php"> Table Status</a>
		</li>
	</ul>
</div>
<?php 
}
?>